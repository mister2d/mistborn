#!/bin/bash

CA_FOLDER="/usr/share/ca-certificates/"
KEY_FOLDER="./tls/"
CACRT_FILE="trusted-ca.crt"
CRT_FILE="cert.crt"
KEY_FILE="cert.key"

CACRT_PATH="$KEY_FOLDER/$CACRT_FILE"
CRT_PATH="$KEY_FOLDER/$CRT_FILE"
KEY_PATH="$KEY_FOLDER/$KEY_FILE"

# ensure openssl installed
sudo apt-get install -y jq openssl

# make folder
mkdir -p $KEY_FOLDER

# generate crt and key
#openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:4096 -keyout $KEY_PATH -out $CRT_PATH -subj "/C=US/ST=Maryland/L=East/O=home/OU=mistborn/CN=*.mistborn.lan/emailAddress=mistborn@localhost"
# Read existing cert/key JSON blob
sudo cat /root/signed_cert_output.json | \
         tee >(jq -j .privateKey > $KEY_PATH) \
             >(jq -j .certificate > $CRT_PATH) \
             >(jq -j .caChain > $CACRT_PATH)


# set permissions
chmod 644 $CRT_PATH
chmod 644 $CACRT_PATH
chmod 600 $KEY_PATH

# Update trusted CA store
sudo cp $CACRT_PATH $CA_FOLDER
sudo /usr/sbin/update-ca-certificates
